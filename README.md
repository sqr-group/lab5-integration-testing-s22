# Lab5 -- Integration testing


## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
Key for spring semester in 2022 is `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. Try to use InnoDrive application:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it. 
- Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our default spec(your result might be different):
        Here is InnoCar Specs:

        Budet car price per minute = 17

        Luxury car price per minute = 33

        Fixed price per km = 11

        Allowed deviations in % = 10

        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_` with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 


### Specifications of InnoCar:

Here is InnoCar Specs:
Budet car price per minute = 15
Luxury car price per minute = 41
Fixed price per km = 13
Allowed deviations in % = 12
Inno discount in % = 13

I will divide 
`time`, `planned_time`, `distance`, and `planned_distance` into similar equivalence classes:

* `<= 0` (considering invalid)
* `> 0`

`type` will have 3 equivalence classes:

* `budget`
* `luxury`
* `nonsense`

`plan` can be divided into:

* `minute`
* `fixed_price`
* `nonsense`

`inno_discount` 's BVA:

* `yes`
* `no`
* `nonsense`

### BVA:

| Parameter          | Equivalence Classes                 |
| ------------------ | ----------------------------------- |
| `distance`         | `<=0`, `>0`                         |
| `planned_distance` | `<=0`, `>0`                         |
| `time`             | `<=0`, `>0`                        |
| `planned_time`     | `<=0`, `>0`                         |
| `type`             | `budget`, `luxury`, `nonsense`      |
| `plan`             | `minute`, `fixed_price`, `nonsense` |
| `inno_discount`    | `yes`, `no`, `nonsense`             |

In the decision table \* represent anything.

### Decision table




| Conditions (inputs) | Values                              |  R1   |  R2   |  R3         |  R4        |     R5      |     R6        |     R7        |      R8       |     R9     |   R10    |   R11    |   R12    |      R13      |      R14      |
|---------------------|-------------------------------------|:-----:|:-----:|:-----------:|:----------:|:-----------:|:-------------:|:-------------:|:-------------:|:----------:|:--------:|:--------:|:--------:|:-------------:|:-------------:|
| `type`              | `budget`, `luxury`, `nonsense`      |   *   |   *   |   *         | `nonsense` |     *       |     *         |     *         |   `luxury`    |  `luxury`  | `luxury` | `budget` | `budget` |   `budget`    |   `budget`    |
| `plan`              | `minute`, `fixed_price`, `nonsense` |   *   |   *   | `nonsense`  |   *        |     *       | `fixed_price` | `fixed_price` | `fixed_price` |  `minute`  | `minute` | `minute` | `minute` | `fixed_price` | `fixed_price` |
| `distance`          | `<= 0`, `> 0`                       | `<=0` |  `>0` | `>0`        |  `>0`      |    `>0`     |    `>0`       |    `>0`       |     `>0`      |     `>0`   |   `>0`   |   `>0`   |   `>0`   |     `>0`      |     `>0`      |
| `planned_distance`  | `<= 0`, `> 0`                       |   *   |   *   |   *         |   *        |     *       |    '<=0'      |    `>0`       |     `>0`      |     `>0`   |   `>0`   |   `>0`   |   `>0`   |     `>0`      |     `>0`      |
| `time`              | `<= 0`, `> 0`                       |   *   | `<=0  | `>0`        |  `>0`      |    `>0`     |    `>0`       |    `>0`       |     `>0`      |     `>0`   |   `>0`   |   `>0`   |   `>0`   |     `>0`      |     `>0`      |
| `planned_time`      | `<= 0`, `> 0`                       |   *   |   *   |   *         |   *        |     *       |     *         |    `<=0`      |     `>0`      |     `>0`   |   `>0`   |   `>0`   |   `>0`   |     `>0`      |     `>0`      |
| `inno_discount`     | `yes`, `no`, `nonsense`             |   *   |   *   |   *         |   *        | `nonsense`  |     *         |     *         |       *       |    `no`    |  `yes`   |  `no`    |   `yes`  |     `no`      |     `yes`     |
|---------------------|-------------------------------------| ------| ------| ------------| -----------|-------------|---------------|---------------|---------------|------------|----------|----------|----------|---------------|---------------|
| `Invalid Request`      |                                  |   X   |   X   |   X         |   X        |     X       |     X         |     X         |       X       |            |          |          |          |               |               |
| 200                 |                                     |       |       |             |            |             |               |               |               |     X      |     X    |    X     |    X     |       X       |       X       |


In test cases -10 and 0 reyes `<=0` class, 1 and 10 represent `>0` equivalence class. 


### Test Cases

> Bugs are written in bold (see Expected Result and Actual Result).

| ID  | Desition Table Entry | `distance` |  `time`        | `plan`        | `type`             | `inno_discount` | `planned_distance` | `planned_time`  | Expected Result     | Actual Result          |
|-----|----------------------|------------|----------------|---------------|--------------------|-----------------|--------------------|-----------------|---------------------|------------------------|
| 0   | R1                   | -10        | 1              | `minute`      | `budget`           | `no`            | -10                | 1               | Invalid Request     | Invalid Request        |
| 1   | R1                   | 0          | 1              | `minute`      | `budget`           | `no`            | 0                  | 1               | Invalid Request     | 16                     | **Wrong**
| 2   | R2                   | 1          | -10            | `minute`      | `budget`           | `no`            | 1                  | -10             | Invalid Request     | Invalid Request        |
| 3   | R2                   | 10         | 0              | `minute`      | `budget`           | `no`            | 10                 | 0               | Invalid Request     | 0                      | **Wrong**
| 4   | R3                   | 1          | 1              | `nonsense`    | `budget`           | `no`            | 1                  | 1               | Invalid Request     | Invalid Request        |
| 5   | R4                   | 1          | 1              | `minute`      | `nonsense`         | `no`            | 1                  | 1               | Invalid Request     | Invalid Request        |
| 6   | R5                   | 1          | 1              | `minute`      | `budget`           | `nonsense`      | 1                  | 1               | Invalid Request     | Invalid Request        |
| 7   | R6                   | 1          | 1              | `fixed_price` | `budget`           | `no`            | 0                  | 1               | Invalid Request     | 16.6666666             | **Wrong**
| 8   | R6                   | 1          | 1              | `fixed_price` | `budget`           | `no`            | -10                | 1               | Invalid Request     | Invalid Request        | 
| 9   | R7                   | 1          | 1              | `fixed_price` | `budget`           | `no`            | 1                  | 0               | Invalid Request     | 16.6666666             | **Wrong**
| 10  | R7                   | 1          | 1              | `fixed_price` | `budget`           | `no`            | 1                  | -10             | Invalid Request     | 16.6666666             | **Wrong**
| 11  | R8                   | 1          | 1              | `fixed_price` | `luxury`           | `no`            | 1                  | 1               | Invalid Request     | Invalid Request        |
| 12  | R9                   | 1          | 1              | `minute`      | `luxury`           | `no`            | 1                  | 1               | 41                  | 41                     |
| 13  | R9                   | 10         | 1              | `minute`      | `luxury`           | `no`            | 10                 | 1               | 41                  | 41                     |
| 14  | R9                   | 1          | 10             | `minute`      | `luxury`           | `no`            | 1                  | 10              | 410                 | 410                    |
| 15  | R9                   | 10         | 10             | `minute`      | `luxury`           | `no`            | 10                 | 10              | 410                 | 410                    |
| 16  | R10                  | 1          | 1              | `minute`      | `luxury`           | `yes`           | 1                  | 1               | 35.67               | 35.67                  |
| 17  | R10                  | 10         | 1              | `minute`      | `luxury`           | `yes`           | 10                 | 1               | 35.67               | 35.67                  |
| 18  | R10                  | 1          | 10             | `minute`      | `luxury`           | `yes`           | 1                  | 10              | 356.7               | 356.7                  |
| 19  | R10                  | 10         | 10             | `minute`      | `luxury`           | `yes`           | 10                 | 10              | 356.7               | 356.7                  |
| 20  | R11                  | 1          | 1              | `minute`      | `budget`           | `no`            | 1                  | 1               | 15                  | 16                     | **Wrong**
| 21  | R11                  | 10         | 1              | `minute`      | `budget`           | `no`            | 10                 | 1               | 15                  | 16                     | **Wrong**
| 22  | R11                  | 1          | 10             | `minute`      | `budget`           | `no`            | 1                  | 10              | 150                 | 160                    | **Wrong**
| 23  | R11                  | 10         | 10             | `minute`      | `budget`           | `no`            | 10                 | 10              | 150                 | 160                    | **Wrong**
| 24  | R12                  | 1          | 1              | `minute`      | `budget`           | `yes`           | 1                  | 1               | 13.05               | 13.92                  | **Wrong**
| 25  | R12                  | 10         | 1              | `minute`      | `budget`           | `yes`           | 10                 | 1               | 13.05               | 13.92                  | **Wrong**
| 26  | R12                  | 1          | 10             | `minute`      | `budget`           | `yes`           | 1                  | 10              | 130.5               | 139.2                  | **Wrong**
| 27  | R12                  | 10         | 10             | `minute`      | `budget`           | `yes`           | 10                 | 10              | 130.5               | 139.2                  | **Wrong**
| 28  | R13                  | 1          | 1              | `fixed_price` | `budget`           | `no`            | 1                  | 1               | 13                  | 12.5                   | **Wrong**
| 29  | R13                  | 10         | 1              | `fixed_price` | `budget`           | `no`            | 10                 | 1               | 130                 | 125                    | **Wrong**
| 30  | R13                  | 1          | 10             | `fixed_price` | `budget`           | `no`            | 1                  | 10              | 13                  | 12.5                   | **Wrong**
| 31  | R13                  | 10         | 10             | `fixed_price` | `budget`           | `no`            | 10                 | 10              | 130                 | 125                    | **Wrong**
| 32  | R14                  | 1          | 1              | `fixed_price` | `budget`           | `yes`           | 1                  | 1               | 11.31               | 10.875                 | **Wrong**
| 33  | R14                  | 10         | 1              | `fixed_price` | `budget`           | `yes`           | 10                 | 1               | 113.1               | 108.75                 | **Wrong**
| 34  | R14                  | 1          | 10             | `fixed_price` | `budget`           | `yes`           | 1                  | 10              | 11.31               | 10.875                 | **Wrong**
| 35  | R14                  | 10         | 10             | `fixed_price` | `budget`           | `yes`           | 10                 | 10              | 113.1               | 108.75                 | **Wrong**


### Bugs:

ID1 - `0` allowed in distance

ID3 - `0` allowed in time

ID7 - `0` allowed in planned distance

ID9 - `0` allowed in planned time

ID10 - `-10` allowed in planned time

ID20, ID21, ID22, ID23 - budget price is different in spec request and in cound requests

ID24, ID25, ID26, ID27 - budget price is different in spec request and in cound requests

ID28, ID29, ID30, ID31 - budget price is different in spec request and in cound requests

ID32, ID33, ID34, ID35 - budget price is different in spec request and in cound requests